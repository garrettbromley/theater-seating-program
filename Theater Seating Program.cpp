/*
	File: Theater Seating Program.cpp
	Author: Garrett Bromley
	Last Revised: 5/27/2017
	
	Description:
		- Can be used by a small theater to sell tickets for performances. 
		- The theater's auditorium has 10 rows of seats with 9 seats in each row. 
		- The program has a screen that shows which seats are available and which are taken. 
			- Taken: *
			- Available: #
		- Every time a ticket or group of tickets is purchased, total ticket prices and updated seating chart is displayed.
		- The program keeps a total of all ticket sales.
		- Can see the following statistics:
			- How many seats have been sold
			- How many seats are available in each row
			- How many seats are available in the entire auditorium.
*/

//------------------------------//
// Includes						//
//------------------------------//
#include "stdafx.h"
#include <iostream>
#include <iomanip>
using namespace std;

#include "Theater.h"

//------------------------------//
// Constants					//
//------------------------------//
#define T_ROWS 10				// Theater Rows
#define T_SEATS_PER_ROW 9		// Theater Seats Per Row
#define TICKET_PRICE 20			// Price of a ticket [$]
#define SEPERATOR '|'			// Vertical Seperator to be used in table
#define LINE '='				// Horizontal Line to be used in table

//------------------------------//
// Functions					//
//------------------------------//

// Returns the number of digits in a number
int Digits(int num) {
	// The "length" of 0 is 1:
	int len = 1;

	// And for numbers greater than 0:
	if (num > 0) {
		// We count how many times it can be divided by 10:
		// (how many times we can cut off the last digit until we end up with 0)
		for (len = 0; num > 0; len++) {
			num = num / 10;
		}
	}

	return len;
}

// Asks the user what they want to do
int DisplayMenu() {
	int input = -1;

	system("CLS");
	
	// Display menu
	cout << SEPERATOR << setw(28) << setfill(LINE)		<< SEPERATOR << "\n";
	cout << SEPERATOR << " Main Menu:                "	<< SEPERATOR << endl;
	cout << SEPERATOR << setw(28) << setfill(LINE)		<< SEPERATOR << "\n";
	cout << SEPERATOR << " 1 - Display Seating Chart "	<< SEPERATOR << endl;
	cout << SEPERATOR << " 2 - Purchase Tickets      "	<< SEPERATOR << endl;
	cout << SEPERATOR << " 3 - Manager Details       "	<< SEPERATOR << endl;
	cout << SEPERATOR << " 0 - Exit                  "	<< SEPERATOR << endl;
	cout << SEPERATOR << setw(28) << setfill(LINE)		<< SEPERATOR << "\n";
	cout << "Selection: ";
	cin >> input;

	// If they enter an invalid selection, keep requesting
	while (input < 0 || input > 3) {
		cin.clear();
		cin.ignore();
		cout << "Invalid Selection. Please Try Again!" << endl;
		cout << "Selection: ";
		cin >> input;
	}

	return input;
}

// Processes the user's decision from the main menu
void ProcessMenuInput(int input, Theater &theTheater) {
	switch (input) {
		case 1: { theTheater.PrintSeatingChart();	break; };
		case 2: { theTheater.PurchaseTickets();		break; };
		case 3: { theTheater.PrintManagerDetails(); break; };
	}

	system("PAUSE");
}

//------------------------------//
// Main							//
//------------------------------//
int main() {
	// Pre-Launch Check for maximum size of theater
	if (Digits(T_ROWS) > 2) {			cout << "TOO MANY ROWS. EXITING... ["			<< T_ROWS			<< "], Limit of 99" << endl; return 0; }
	if (Digits(T_SEATS_PER_ROW) > 2) {	cout << "TOO MANY SEATS PER ROW. EXITING... ["	<< T_SEATS_PER_ROW	<< "], Limit of 99" << endl; return 0; }

	// Setup Program
	Theater myTheater(T_ROWS, T_SEATS_PER_ROW, TICKET_PRICE);	// Our Theater
	int userInput = 0;											// Holds the user's input

	// While we want to run the program
	while (true) {
		// Display the menu and store their input
		userInput = DisplayMenu();

		// If the user wants to exit, break the loop
		if (userInput == 0) { break; };
		
		// Process their input
		ProcessMenuInput(userInput, myTheater);
	}
	
	return 0;
}

