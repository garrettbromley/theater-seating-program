#include "stdafx.h"
#include "Theater.h"
#include <iostream>
#include <iomanip>
#include <vector>

//------------------------------//
// Constants					//
//------------------------------//
#define SEAT_TAKEN "*"			// Designation for taken seats
#define SEAT_AVAIL "#"			// Designation for available seats
#define SEPERATOR '|'			// Vertical Seperator to be used in table
#define LINE '='				// Horizontal Line to be used in table

//------------------------------//
// Functions					//
//------------------------------//

// Returns the number of digits in a number
int TheaterDigits(int num) {
	num++;	// Add 1

	// The "length" of 0 is 1:
	int len = 1;

	// And for numbers greater than 0:
	if (num > 0) {
		// We count how many times it can be divided by 10:
		// (how many times we can cut off the last digit until we end up with 0)
		for (len = 0; num > 0; len++) {
			num = num / 10;
		}
	}

	return len;
}

// Initialization
Theater::Theater(int rows, int seats, int ticketCost)
{
	this->rows = rows;						// Set Rows
	this->seatsPerRow = seats;				// Set Seats Per Row
	this->ticketCost = ticketCost;			// Set Ticket Cost
	this->seatsAvailable = rows * seats;	// Set Total Seats Available
	this->seatsSold = 0;					// Set Total Seats Sold
	this->totalSales = 0;					// Set Total Sales

	// Initialize seating chart to all available
	this->seatingChart.resize(rows, vector<bool>(seats, false));
}

// Print out the seating chart to the user
void Theater::PrintSeatingChart()
{
	system("CLS");

	// If we have more than 9 columns, adjust the line seperator length
	int overflow = 0;
	if (this->seatsPerRow > 9) { overflow = this->seatsPerRow - 9; }
	if (this->seatsPerRow < 9) { overflow = -(9 - this->seatsPerRow); }

	// Print the header
	cout << setw(this->seatsPerRow * 5 - overflow) << setfill(LINE) << "\n";		// Top Line
	cout << SEPERATOR << "     " << SEPERATOR;						// Blank Corner
	for (int hCol = 0; hCol < this->seatsPerRow; ++hCol) {			// Column Titles
		if (TheaterDigits(hCol) > 1) {
			cout << SEPERATOR << " " << hCol + 1;						// Take out space for >= 10 columns
		}
		else {
			cout << SEPERATOR << " " << hCol + 1 << " ";				// Have space for 1 to 9 columns
		}
	}
	cout << SEPERATOR << "\n";										// End of Column Titles
	cout << setw(this->seatsPerRow * 5 - overflow) << setfill(LINE) << "\n";		// Seperator Line

																				// Loop through the rows
	for (int row = 0; row < this->rows; ++row) {
		// Print the row prefix
		if (TheaterDigits(row) > 1) {
			cout << SEPERATOR << " R " << row + 1 << SEPERATOR;			// Take out space for >= 10 rows
		}
		else {
			cout << SEPERATOR << " R " << row + 1 << " " << SEPERATOR;	// Have space for 1 to 9 rows
		}

		// Loop through the columns
		for (int col = 0; col < this->seatsPerRow; ++col) {
			if (this->seatingChart[row][col]) {
				cout << SEPERATOR << " " << SEAT_TAKEN << " ";		// Seat is taken
			}
			else {
				cout << SEPERATOR << " " << SEAT_AVAIL << " ";		// Seat is available
			}
		}

		// End of row
		cout << SEPERATOR << endl;
	}

	// Print the bottom of the table
	cout << setw(this->seatsPerRow * 5 - overflow) << setfill(LINE) << "\n";

	cout << "There are " << this->seatsAvailable << " seats left to purchase." << endl;
}

void Theater::PrintManagerDetails()
{
	system("CLS");
	
	// Print the useful information to the manager
	cout << "Seats Sold: " << this->seatsSold << " of " << (this->rows * this->seatsPerRow) << endl;
	cout << "Seats Left: " << this->seatsAvailable << " of " << (this->rows * this->seatsPerRow) << endl;
	cout << "Total Sales: $" << this->totalSales << endl;
}

void Theater::PurchaseTickets()
{
	int tickets = -1;

	// Display the current seating
	this->PrintSeatingChart();

	// Ask them how many tickets they need
	cout << "How many tickets do you wish to purchase: ";
	cin >> tickets;

	// If they enter an invalid selection, keep requesting
	while (tickets < 0 || tickets > this->seatsAvailable) {
		cin.clear();
		cin.ignore();
		cout << "Invalid Number. Please Try Again!" << endl;
		cout << "How many tickets do you wish to purchase: ";
		cin >> tickets;
	}

	// For each ticket they need
	for (int i = 0; i < tickets; i++)
	{
		int row = -1; int seat = -1; 
		bool validSeat = false;

		while (!validSeat) {
			// Display the seating chart
			this->PrintSeatingChart();

			// Ask which row they want to sit in
			cout << "Which row would you like to sit in: ";
			cin >> row;

			while (row < 1 || row > this->rows) {
				cin.clear();
				cin.ignore();
				cout << "Invalid Row. Please Try Again!" << endl;
				cout << "Which row would you like to sit in: ";
				cin >> row;
			}

			// Ask which seat they want
			cout << "Which seat in row " << row << " would you like to sit in: ";
			cin >> seat;

			while (seat < 1 || seat > this->seatsPerRow) {
				cin.clear();
				cin.ignore();
				cout << "Invalid Seat. Please Try Again!" << endl;
				cout << "Which seat in row " << row << " would you like to sit in: ";
				cin >> seat;
			}

			// If it is available, claim it
			if (this->SeatAvailable(row, seat)) {
				this->PurchaseSeat(row, seat);
				cout << "Ticket(s) Purchased for $" << (tickets * this->ticketCost) << endl;
				validSeat = true;
			} else {
				cin.clear();
				cin.ignore();
				cout << "This seat is taken! Please choose another seat." << endl;
				system("PAUSE");
			}
		}
	}
}

bool Theater::SeatAvailable(int row, int seat)
{
	if (this->seatingChart[row - 1][seat - 1]) { 
		return false; 
	} else { 
		return true; 
	}
}

void Theater::PurchaseSeat(int row, int seat)
{
	this->seatingChart[row - 1][seat - 1] = true;
	this->seatsAvailable--;
	this->seatsSold++;
	this->totalSales += this->ticketCost;
}

void Theater::SetTicketCost(int cost)
{
	this->ticketCost = cost;
}

void Theater::SetAvailableSeats(int seats)
{
	this->seatsAvailable = seats;
}

void Theater::SetSoldSeats(int seats)
{
	this->seatsSold = seats;
}

void Theater::SetTotalSales(int total)
{
	this->totalSales = total;
}

Theater::~Theater()
{
}
