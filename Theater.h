#pragma once
#include <vector>
using namespace std;

class Theater
{
public:
	Theater(int r, int s, int cost);
	~Theater();
	void PrintSeatingChart();
	void PrintManagerDetails();
	void PurchaseTickets();
	bool SeatAvailable(int row, int seat);
	void PurchaseSeat(int row, int seat);
	void SetTicketCost(int cost);
	void SetAvailableSeats(int seats);
	void SetSoldSeats(int seats);
	void SetTotalSales(int total);
private:
	vector<vector<bool>> seatingChart;
	int rows;
	int seatsPerRow;
	int seatsAvailable;
	int seatsSold;
	int ticketCost;
	int totalSales;
};

